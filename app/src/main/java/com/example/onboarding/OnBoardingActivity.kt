package com.example.onboarding

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.aemerse.onboard.*
import com.example.onboarding.base.RectangleIndicatorController

class OnBoardingActivity : OnboardAdvanced() {
    override val layoutId = R.layout.onboarding_container
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.indicatorController = RectangleIndicatorController(this)
        // Make sure you don't call setContentView!
        // Call addSlide passing your Fragments.
        // You can use OnboardFragment to use a pre-built fragment
        addSlide(
            OnboardCustomLayoutFragment.newInstance(R.layout.fragment_on_boarding)
        )
        addSlide(
            OnboardCustomLayoutFragment.newInstance(R.layout.fragment_on_boarding_2)
        )
        addSlide(
            OnboardCustomLayoutFragment.newInstance(R.layout.fragment_on_boarding_3)
        )
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        notFirstTime()
        startActivity(
            Intent(
                this, MainActivity::class.java
            )
        )
        finish()
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        notFirstTime()
        startActivity(
            Intent(
                this, MainActivity::class.java
            )
        )
        finish()
    }

    fun notFirstTime(){
        val sharedPreferences = getSharedPreferences("MySharedPref", MODE_PRIVATE)
        val myEdit = sharedPreferences.edit()
        myEdit.putBoolean("NotFirstTime",true)
        myEdit.commit()
    }
}