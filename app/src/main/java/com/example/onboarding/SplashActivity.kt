package com.example.onboarding

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen


class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()
        super.onCreate(savedInstanceState)
        val sharedPreferences = getSharedPreferences("MySharedPref", MODE_PRIVATE)
        val notfirsttime = sharedPreferences.getBoolean("NotFirstTime",false)
        if(notfirsttime){
            startActivity(
                Intent(
                    this@SplashActivity, MainActivity::class.java
                )
            )
            finish()
        }else{
            startActivity(
                Intent(
                    this@SplashActivity, OnBoardingActivity::class.java
                )
            )
            finish()
        }
    }
}